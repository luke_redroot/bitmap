# frozen_string_literal: true

require 'spec_helper'

describe NewBitmapCommand do
  it_behaves_like 'a registered command'

  it 'should match valid lines and ignore invalid lines with bad params' do
    matcher = Command.commands[NewBitmapCommand]
    expect(matcher.match('I 4 5').to_a).to eql(['I 4 5', '4', '5'])
    expect(matcher.match('I 10 100').to_a).to eql(['I 10 100', '10', '100'])
    expect(matcher.match('I AAA AAA')).to eql(nil)
  end

  it 'should return a new bitmap with specified size' do
    bitmap = NewBitmapCommand.new(4, 8).perform(nil)
    expect(bitmap.width).to eql(4)
    expect(bitmap.height).to eql(8)
  end

  it 'should not raise a missing bitmap error if none supplied' do
    expect do
      NewBitmapCommand.new.perform(nil)
    end.not_to raise_error(Command::MissingBitmapError)
  end
end
