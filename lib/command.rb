# frozen_string_literal: true

class Command
  class MissingBitmapError < StandardError; end

  class << self
    def commands
      @@commands ||= {}
    end

    def matching(regex)
      commands[self] = Regexp.new(regex)
    end
  end

  def perform(_)
    raise NotImplementedError
  end
end

# load all commands
Dir[File.join(File.dirname(__FILE__), 'commands', '*')].each { |f| require f }
