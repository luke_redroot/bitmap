# frozen_string_literal: true

describe ColourCommand do
  it_behaves_like 'a registered command'

  it 'should match valid lines and ignore bad params' do
    matcher = Command.commands[ColourCommand]
    expect(matcher.match('L 4 5 X').to_a).to eql(['L 4 5 X', '4', '5', 'X'])
    expect(matcher.match('L 4 5')).to eql(nil)
    expect(matcher.match('L A 5 5')).to eql(nil)
  end

  it 'should colour the pixel the specified colour' do
    bitmap = Bitmap.new(4, 4)
    ColourCommand.new(1, 1, 'A').perform(bitmap)
    expect(bitmap[1, 1]).to eql('A')
  end

  it 'should raise a missing bitmap error if none supplied' do
    expect do
      ColourCommand.new(1, 1, 'A').perform(nil)
    end.to raise_error(Command::MissingBitmapError)
  end
end
