# frozen_string_literal: true

class HorizontalCommand < Command
  matching /^H (\d+) (\d+) (\d+) ([A-Z])$/

  def initialize(x1, x2, y, colour)
    @x1 = x1.to_i
    @x2 = x2.to_i
    @y = y.to_i
    @colour = colour
  end

  def perform(bitmap)
    raise MissingBitmapError if bitmap.nil?
    min, max = [@x1, @x2].minmax
    (min..max).each do |x|
      bitmap[x, @y] = @colour
    end
    bitmap
  end
end
