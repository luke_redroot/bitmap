# frozen_string_literal: true

require 'spec_helper'

describe ClearCommand do
  it_behaves_like 'a registered command'

  it 'should match valid lines with no params' do
    matcher = Command.commands[ClearCommand]
    expect(matcher.match('C')).to be_a_kind_of(MatchData)
    expect(matcher.match('C 10')).to eql(nil)
  end

  it 'should remove all colours in grid' do
    bitmap = Bitmap.new(3, 3)
    bitmap[1, 1] = 'A'
    bitmap[2, 1] = 'B'
    ClearCommand.new.perform(bitmap)
    all_cells_blank = bitmap.grid.flatten.all? do |c|
      c.eql?(Bitmap::EMPTY_GRID_CHARACTER)
    end
    expect(all_cells_blank).to eql(true)
  end

  it 'should raise a missing bitmap error if none supplied' do
    expect do
      ClearCommand.new.perform(nil)
    end.to raise_error(Command::MissingBitmapError)
  end
end
