# frozen_string_literal: true

require 'byebug'

require File.join(File.dirname(__FILE__), '..', 'lib', 'bitmap_editor')
require File.join(File.dirname(__FILE__), '..', 'lib', 'bitmap')
require File.join(File.dirname(__FILE__), '..', 'lib', 'command')

require File.join(File.dirname(__FILE__), 'support', 'registered_command')

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
    expectations.warn_about_potential_false_positives = false
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  config.color = true
  config.filter_run focus: true
  config.run_all_when_everything_filtered = true
end
