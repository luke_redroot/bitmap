# Bitmap editor

Let's draw some images!

# Running

    `>bin/bitmap_editor examples/show.txt`

By default erroneous commands will output error messages to stdout, but you can
configure this using the environment variable `ERROR_MODE`. By default the value is `warn`
but you can force the raising of command errors by setting it to `failure`

    `>ERROR_MODE=failure bin/bitmap_editor examples/broken.txt`

# Tests

    bundle exec rspec
    bundle exec rubocop # for linting
