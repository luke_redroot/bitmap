# frozen_string_literal: true

require 'spec_helper'

describe Bitmap do
  it 'expects Bitmap to define constants' do
    expect(Bitmap::EMPTY_GRID_CHARACTER).to eql('0')
    expect(Bitmap::MAX_SIZE).to eql(250)
    expect(Bitmap::MIN_SIZE).to eql(1)
    expect(Bitmap::ALLOWED_VALUES).to be_a_kind_of(Regexp)
  end

  it 'can be initialized with a size, with correct grid format' do
    bitmap = Bitmap.new(3, 4)
    expect(bitmap.width).to eql(3)
    expect(bitmap.height).to eql(4)
    expect(bitmap.grid).to be_a_kind_of(Array)
    expect(bitmap.grid).to eql([
      %w[0 0 0],
      %w[0 0 0],
      %w[0 0 0],
      %w[0 0 0]
    ])
  end

  it 'can be initialised with a grid size between 1 and 250 inclusive' do
    expect { Bitmap.new(1, 1) }.not_to raise_error(Bitmap::GridSizeLimitError)
    expect { Bitmap.new(250, 250) }.not_to raise_error(Bitmap::GridSizeLimitError)
  end

  it 'throws an error if the grid size is beyond outside of 1 and 250' do
    expect { Bitmap.new(-1, 1) }.to raise_error(Bitmap::GridSizeLimitError)
    expect { Bitmap.new(1, 400) }.to raise_error(Bitmap::GridSizeLimitError)
  end

  it 'correctly verifies if a pixel position is within bounds' do
    bitmap = Bitmap.new(3, 4)
    expect(bitmap.valid?(1, 1)).to eql(true)
    expect(bitmap.valid?(1, 4)).to eql(true)
    expect(bitmap.valid?(4, 5)).to eql(false)
    expect(bitmap.valid?(0, 0)).to eql(false)
    expect(bitmap.valid?(-1, -1)).to eql(false)
  end

  it 'allows you to get and set a pixel colour' do
    bitmap = Bitmap.new(3, 4)
    expect(bitmap[1, 1]).to eql('0')
    bitmap[1, 1] = 'A'
    expect(bitmap[1, 1]).to eql('A')
  end

  it 'validates pixel colour is a valid value' do
    bitmap = Bitmap.new(5, 5)
    expect { bitmap[2, 2] = 'A' }.not_to raise_error(Bitmap::InvalidColourError)
    expect { bitmap[2, 2] = 'AAAA' }.to raise_error(Bitmap::InvalidColourError)
    expect { bitmap[2, 2] = 123 }.to raise_error(Bitmap::InvalidColourError)
  end

  it 'throws an error if attempting to get an out of bound pixel' do
    bitmap = Bitmap.new(5, 5)
    expect { bitmap[5, 5] }.not_to raise_error(Bitmap::OutOfBoundsError)
    expect { bitmap[6, 5] }.to raise_error(Bitmap::OutOfBoundsError)
    expect { bitmap[5, 6] }.to raise_error(Bitmap::OutOfBoundsError)
  end

  it 'throws an error if attempting to colour an out of bound pixel' do
    bitmap = Bitmap.new(5, 5)
    expect { bitmap[5, 5] = 'A' }.not_to raise_error(Bitmap::OutOfBoundsError)
    expect { bitmap[6, 5] = 'B' }.to raise_error(Bitmap::OutOfBoundsError)
    expect { bitmap[5, 6] = 'C' }.to raise_error(Bitmap::OutOfBoundsError)
  end
end
