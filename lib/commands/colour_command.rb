# frozen_string_literal: true

class ColourCommand < Command
  matching /^L (\d+) (\d+) ([A-Z])$/

  def initialize(x, y, colour)
    @x = x.to_i
    @y = y.to_i
    @colour = colour
  end

  def perform(bitmap)
    raise MissingBitmapError if bitmap.nil?
    bitmap[@x, @y] = @colour
    bitmap
  end
end
