# frozen_string_literal: true

describe ShowCommand do
  it_behaves_like 'a registered command'

  it 'matches S with no params correctly' do
    matcher = Command.commands[ShowCommand]
    expect(matcher.match('S')).to be_a_kind_of(MatchData)
    expect(matcher.match('S 10')).to eql(nil)
  end

  it 'outputs the grid to stdout' do
    bitmap = Bitmap.new(3, 3)
    command = ShowCommand.new
    expected = "000\n000\n000\n"
    expect { command.perform(bitmap) }.to output(expected).to_stdout
  end

  it 'should raise a missing bitmap error if none supplied' do
    expect do
      ShowCommand.new.perform(nil)
    end.to raise_error(Command::MissingBitmapError)
  end
end
