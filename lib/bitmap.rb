# frozen_string_literal: true

# NB: bitmap is indexed from 1, but internal is an array so indexed from 0
class Bitmap
  class GridSizeLimitError < StandardError; end
  class InvalidColourError < StandardError; end
  class OutOfBoundsError < StandardError; end

  EMPTY_GRID_CHARACTER = '0'
  MIN_SIZE = 1
  MAX_SIZE = 250
  ALLOWED_VALUES = /^[A-Z]{1}$/

  attr_reader :width, :height, :grid

  def initialize(width, height)
    raise GridSizeLimitError unless width >= MIN_SIZE && width <= MAX_SIZE
    raise GridSizeLimitError unless height >= MIN_SIZE && height <= MAX_SIZE

    @width = width
    @height = height
    @grid = Array.new(@height) do
      Array.new(@width, EMPTY_GRID_CHARACTER)
    end
  end

  def valid?(x, y)
    x >= 1 && y >= 1 && x <= @width && y <= @height
  end

  def [](x, y)
    raise OutOfBoundsError unless valid?(x, y)
    @grid[y - 1][x - 1]
  end

  def []=(x, y, val)
    raise OutOfBoundsError unless valid?(x, y)
    raise InvalidColourError if ALLOWED_VALUES.match(val.to_s).nil?
    @grid[y - 1][x - 1] = val
  end
end
