# frozen_string_literal: true

describe VerticalCommand do
  it_behaves_like 'a registered command'

  it 'should match valid lines' do
    m = Command.commands[VerticalCommand]
    expect(m.match('V 0 3 2 C').to_a[1..4]).to eql(%w[0 3 2 C])
    expect(m.match('V 10 10 1 D').to_a[1..4]).to eql(%w[10 10 1 D])
    expect(m.match('V D 10 10')).to eql(nil)
    expect(m.match('V 1 1 1 1')).to eql(nil)
  end

  it 'should colour the horizontal line correctly if in bounds' do
    bitmap = Bitmap.new(15, 15)
    command = VerticalCommand.new(5, 7, 10, 'C')
    command.perform(bitmap)
    (7..10).each do |y|
      expect(bitmap[5, y]).to eql('C')
    end
    expect(bitmap[5, 2]).to eql(Bitmap::EMPTY_GRID_CHARACTER)
  end

  it 'should work if y2 < y1' do
    bitmap = Bitmap.new(15, 15)
    command = VerticalCommand.new(5, 10, 6, 'C')
    command.perform(bitmap)
    (6..10).each do |y|
      expect(bitmap[5, y]).to eql('C')
    end
  end

  it 'should raise a missing bitmap error if none supplied' do
    expect do
      VerticalCommand.new(4, 1, 2, 'B').perform(nil)
    end.to raise_error(Command::MissingBitmapError)
  end
end
