# frozen_string_literal: true

class ClearCommand < Command
  matching /^C$/

  def perform(bitmap)
    raise MissingBitmapError if bitmap.nil?
    bitmap.grid.map! do |row|
      row.fill(Bitmap::EMPTY_GRID_CHARACTER)
    end
    bitmap
  end
end
