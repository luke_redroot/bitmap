# frozen_string_literal: true

class VerticalCommand < Command
  matching /^V (\d+) (\d+) (\d+) ([A-Z])$/

  def initialize(x, y1, y2, colour)
    @x = x.to_i
    @y1 = y1.to_i
    @y2 = y2.to_i
    @colour = colour
  end

  def perform(bitmap)
    raise MissingBitmapError if bitmap.nil?
    min, max = [@y1, @y2].minmax
    (min..max).each do |y|
      bitmap[@x, y] = @colour
    end
    bitmap
  end
end
