# frozen_string_literal: true

require 'spec_helper'

describe BitmapEditor do
  before do
    @editor = BitmapEditor.new
  end

  it 'returns a file not found message if no file exists' do
    expect do
      @editor.run('./a/fake/path.txt')
    end.to output("please provide correct file\n").to_stdout
  end

  it 'should loop through each line of a file, pass to process_line in run' do
    file = File.join(File.dirname(__FILE__), '..', 'examples', '1.txt')
    line_count = File.read(file).split("\n").size
    allow(@editor).to receive(:process_line) { true }
    @editor.run(file)
    expect(@editor).to have_received(:process_line).exactly(line_count).times
  end

  it 'should return parse commands from a line correctly with parse_command' do
    expect(@editor.parse_command('I 4 5')).to eql([NewBitmapCommand, %w[4 5]])
    expect(@editor.parse_command('S')).to eql([ShowCommand, []])
  end

  it 'should return [nil, nil] if no matching command is found' do
    expect(@editor.parse_command('UNKNOWN')).to eql([nil, nil])
  end

  it 'should read the default error mode correctly' do
    expect(@editor.error_mode).to eql(BitmapEditor::DEFAULT_ERROR_MODE)
  end

  context 'warn error mode' do
    before { ENV['ERROR_MODE'] = 'warn' }
    after { ENV.delete('ERROR_MODE') }

    it 'should read the error mode correctly' do
      expect(@editor.error_mode).to eql(:warn)
    end

    it 'should only output to stdout if a command error when running' do
      error = "Error processing 'I 1000 1000': Bitmap::GridSizeLimitError\n"
      expect do
        @editor.process_line('I 1000 1000')
      end.to output(error).to_stdout
    end

    it 'should only output to stdout if a command is not recognised' do
      error = "Error processing 'UNKNOWN': BitmapEditor::UnknownCommandError\n"
      expect do
        @editor.process_line('UNKNOWN')
      end.to output(error).to_stdout
    end
  end

  context 'failure error mode' do
    before { ENV['ERROR_MODE'] = 'failure' }
    after { ENV.delete('ERROR_MODE') }

    it 'should read the error mode correctly' do
      expect(@editor.error_mode).to eql(:failure)
    end

    it 'should raise if a command error when running' do
      expect do
        @editor.process_line('I 1000 1000')
      end.to raise_error(Bitmap::GridSizeLimitError)
    end

    it 'should raise if a command is not recognised' do
      expect do
        @editor.process_line('UNKNOWN')
      end.to raise_error(BitmapEditor::UnknownCommandError)
    end
  end
end
