# frozen_string_literal: true

shared_examples 'a registered command' do
  it 'should be registered in Command list' do
    expect(Command.commands[described_class]).to be_a_kind_of(Regexp)
  end

  it 'should not match bad strings' do
    matcher = Command.commands[described_class]
    expect(matcher.match('12312323')).to eql(nil)
    expect(matcher.match('XXXX')).to eql(nil)
  end
end
