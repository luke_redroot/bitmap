# frozen_string_literal: true

require File.join(File.dirname(__FILE__), '..', 'bitmap')

class NewBitmapCommand < Command
  matching /^I (\d+) (\d+)$/

  def initialize(width, height)
    @width = width.to_i
    @height = height.to_i
  end

  def perform(_)
    Bitmap.new(@width, @height)
  end
end
