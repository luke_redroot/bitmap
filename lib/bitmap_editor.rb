# frozen_string_literal: true

require File.join(File.dirname(__FILE__), 'command')

class BitmapEditor
  class UnknownCommandError < StandardError; end
  attr_reader :bitmap

  ERROR_MODES = %i[failure warn].freeze
  DEFAULT_ERROR_MODE = :warn

  def initialize
    @bitmap = nil
  end

  def run(file)
    return puts 'please provide correct file' if file.nil? || !File.exist?(file)
    File.open(file).each do |line|
      process_line(line.chomp)
    end
  end

  def process_line(line)
    command, params = parse_command(line)
    raise UnknownCommandError unless command
    @bitmap = command.new(*params).perform(@bitmap)
  rescue StandardError => e
    process_line_error(e, line)
  end

  def parse_command(line)
    Command.commands.each do |cmd, regex|
      matches = regex.match(line)
      return [cmd, matches[1..-1]] unless matches.nil?
    end
    [nil, nil]
  end

  def error_mode
    mode = ENV.fetch('ERROR_MODE') { DEFAULT_ERROR_MODE }.to_sym
    ERROR_MODES.include?(mode) ? mode : DEFAULT_ERROR_MODE
  end

  def process_line_error(e, line)
    case error_mode
    when :warn
      puts "Error processing '#{line}': #{e.message}"
    else
      raise e
    end
  end
end
