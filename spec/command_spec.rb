# frozen_string_literal: true

require 'spec_helper'

class GoodCommand < Command
  matching /O (\d+) (\d+)/
  def perform(bitmap)
    bitmap
  end
end

class BadCommand < Command; end

describe Command do
  it 'should have a class variable commands which is a hash' do
    expect(Command.commands).to be_a_kind_of(Hash)
  end

  it 'should allow accesses to registered commands' do
    expect(Command.commands[GoodCommand]).to eql(/O (\d+) (\d+)/)
  end

  it 'should raise a not NotImplementedError if perform is not implemented' do
    bitmap = Bitmap.new(3, 3)
    expect { BadCommand.new.perform(bitmap) }.to raise_error(NotImplementedError)
  end
end
