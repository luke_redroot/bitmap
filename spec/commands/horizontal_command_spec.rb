# frozen_string_literal: true

describe HorizontalCommand do
  it_behaves_like 'a registered command'

  it 'should match valid lines' do
    m = Command.commands[HorizontalCommand]
    expect(m.match('H 0 3 2 C').to_a[1..4]).to eql(%w[0 3 2 C])
    expect(m.match('H 10 10 1 D').to_a[1..4]).to eql(%w[10 10 1 D])
    expect(m.match('H D 10 10')).to eql(nil)
    expect(m.match('H 1 1 1 1')).to eql(nil)
  end

  it 'should colour the horizontal line correctly if in bounds' do
    bitmap = Bitmap.new(5, 5)
    command = HorizontalCommand.new(1, 4, 2, 'B')
    command.perform(bitmap)
    (1..4).each do |x|
      expect(bitmap[x, 2]).to eql('B')
    end
    expect(bitmap[5, 2]).to eql(Bitmap::EMPTY_GRID_CHARACTER)
  end

  it 'should work if x2 < x1' do
    bitmap = Bitmap.new(5, 5)
    command = HorizontalCommand.new(4, 1, 2, 'B')
    command.perform(bitmap)
    (1..4).each do |x|
      expect(bitmap[x, 2]).to eql('B')
    end
  end

  it 'should raise a missing bitmap error if none supplied' do
    expect do
      HorizontalCommand.new(4, 1, 2, 'B').perform(nil)
    end.to raise_error(Command::MissingBitmapError)
  end
end
