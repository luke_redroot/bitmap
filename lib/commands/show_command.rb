# frozen_string_literal: true

class ShowCommand < Command
  matching /^S$/

  def perform(bitmap)
    raise MissingBitmapError if bitmap.nil?
    bitmap.grid.each do |row|
      puts row.join('')
    end
    bitmap
  end
end
